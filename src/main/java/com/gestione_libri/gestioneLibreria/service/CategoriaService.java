package com.gestione_libri.gestioneLibreria.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gestione_libri.gestioneLibreria.models.Categoria;

@Service
public class CategoriaService {

	@Autowired
	private EntityManager entMan ;
	
	private Session getSessione() {
		
		return entMan.unwrap(Session.class) ;
		
	}
	
	public Categoria saveCategoria( Categoria objCat ) {
		
		Categoria temp = new Categoria() ;
		temp.setTitolo(objCat.getTitolo());
		temp.setCodice_categoria(objCat.getCodice_categoria());
		
		Session sessione = getSessione() ;
		
		try {
			
			sessione.save(objCat) ;
			
		} catch( Exception e ) {
			
			System.out.println(e.getMessage());
			
			return null ;
			
		}
		
		return temp ;
		
	}
	
	public Categoria findyById( int varId ) {
		
		Session sessione = getSessione() ;
		
		return( Categoria ) sessione
				.createCriteria(Categoria.class)
				.add(Restrictions.eqOrIsNull("id_categoria", varId))
				.uniqueResult() ;
		
	}
	
	public List<Categoria> findAll(){
		
		Session sessione = getSessione() ;
		
		return sessione.createCriteria(Categoria.class).list() ;
		
	}
	
	@Transactional
	public boolean delete( int varId ) {
		
		Session sessione = getSessione() ;
		
		try {
			
			Categoria temp = sessione.load(Categoria.class, varId) ;
			
			sessione.delete(temp) ;
			sessione.flush() ;
			
			return true ;
			
			
		} catch( Exception e) {
			
			System.out.println(e.getMessage());
			
		}
		
		return false ;
		
	}
	
	@Transactional
	public boolean update( Categoria objCategoria ) {
		
		Session sessione = getSessione() ;
		
		try {
			
			Categoria temp = sessione.load(Categoria.class, objCategoria.getId_categoria() ) ;
			
			if( temp != null ) {
				
				temp.setTitolo(objCategoria.getTitolo()) ;
				temp.setCodice_categoria(objCategoria.getCodice_categoria());
				
				sessione.update(temp) ;
				sessione.flush() ;
				
				return true ;
				
			}
			
			} catch( Exception e) {
			
				System.out.println(e.getMessage());
			
			}
		
		return false ;
	}
	
	
}

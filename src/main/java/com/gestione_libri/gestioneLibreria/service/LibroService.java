package com.gestione_libri.gestioneLibreria.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gestione_libri.gestioneLibreria.models.Libro;

@Service
public class LibroService {

	@Autowired
	private EntityManager entMan ;
	
	private Session getSessione() {
		
		return entMan.unwrap(Session.class) ;
		
	}
	
	public Libro saveBook( Libro objLibro ) {
		
		Libro temp = new Libro() ;
		temp.setIsbn(objLibro.getIsbn());
		temp.setTitolo(objLibro.getTitolo());
		temp.setAutore(objLibro.getAutore());
		temp.setCategoria_rif(objLibro.getCategoria_rif());
		
		Session sessione = getSessione() ;
		
		try {
			
			sessione.save(temp) ;
			
		} catch( Exception e ) {
			
			System.out.println(e.getMessage());
			
		}
		
		return temp ;
		
		
	}
	
	public Libro findById ( int varId ) {
		
		Session sessione = getSessione() ;
		
		return( Libro )sessione 
				.createCriteria(Libro.class)
				.add(Restrictions.eqOrIsNull("id_libro", varId))
				.uniqueResult() ;
				
	}
	 
	public List<Libro> findBooks(){
		
		Session sessione = getSessione() ;
		
		return sessione.createCriteria(Libro.class).list() ;
		
	}
	
	@Transactional
	public boolean deleteBook( int varId ) {
		
		Session sessione = getSessione() ;
		
		try {
			
			Libro temp = sessione.load( Libro.class, varId ) ;
			
			sessione.delete(temp) ;
			
			sessione.flush() ;
			
			return true ;
			
		} catch( Exception e ) {
			
			System.out.println( e.getMessage() ); 
			
		}
		
		return false ;
		
	}
	
	@Transactional
	public boolean updateBook( Libro objLibro ) {
		
		Session sessione = getSessione() ;
		
		try {
			
			Libro temp = sessione.load(Libro.class, objLibro.getId_libro()) ;
			
			if( temp != null ) {
				
				temp.setIsbn(objLibro.getIsbn());
				temp.setTitolo(objLibro.getTitolo());
				temp.setAutore(objLibro.getAutore());
				temp.setCategoria_rif(objLibro.getCategoria_rif());
				
				sessione.update(temp) ;
				sessione.flush() ;
				
				return true ;
				
			}
			
		} catch( Exception e ) {
			
			System.out.println( e.getMessage() ); 
			
		}
		
		return false ;
		
	}
	
	
}

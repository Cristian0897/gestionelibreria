package com.gestione_libri.gestioneLibreria.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table( name = "libro" )
public class Libro {
	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	@Column( name = "id_libro")
	private int id_libro ;
	
	@Column( name = "isbn" )
	private String isbn ;
	
	@Column( name = "titolo" )
	private String titolo ;
	
	@Column( name = "autore" )
	private String autore ;
	
	@Column( name = "categoria_rif" )
	private int categoria_rif ;

	public int getId_libro() {
		return id_libro;
	}

	public void setId_libro(int id_libro) {
		this.id_libro = id_libro;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getAutore() {
		return autore;
	}

	public void setAutore(String autore) {
		this.autore = autore;
	}

	public int getCategoria_rif() {
		return categoria_rif;
	}

	public void setCategoria_rif(int categoria_rif) {
		this.categoria_rif = categoria_rif;
	}
	
	

}

package com.gestione_libri.gestioneLibreria.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table( name = "categoria" )
public class Categoria {

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	@Column( name = "id_categoria")
	private int id_categoria ;
	
	@Column( name = "titolo" )
	private String titolo ;
	
	@Column( name = "codice_categoria")
	private String codice_categoria ;

	public int getId_categoria() {
		return id_categoria;
	}

	public void setId_categoria(int id_categoria) {
		this.id_categoria = id_categoria;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getCodice_categoria() {
		return codice_categoria;
	}

	public void setCodice_categoria(String codice_categoria) {
		this.codice_categoria = codice_categoria;
	}
	
}

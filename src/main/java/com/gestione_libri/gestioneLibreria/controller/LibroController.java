package com.gestione_libri.gestioneLibreria.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gestione_libri.gestioneLibreria.models.Libro;
import com.gestione_libri.gestioneLibreria.service.LibroService;

@RestController
@RequestMapping("/libro")
@CrossOrigin(origins="*", allowedHeaders="*")
public class LibroController {

	@Autowired
	private LibroService service ;
	
	@GetMapping("/test")
	public String test() {
		return "HelloWorld!" ;
	}

	@PostMapping("/inserisciLibro")
	public Libro aggiungi_libro( @RequestBody Libro varLibro ) {
		
		return service.saveBook(varLibro) ;
		
	}
	
	@GetMapping("/{id_libro}")
	public Libro trovaLibro( @PathVariable  int id_libro) {
		
		return service.findById(id_libro) ;
		
	}
	
	@GetMapping("/")
	public List<Libro> mostraLibri(){
		
		return service.findBooks() ;
		
	}
	
	@DeleteMapping("/{id_libro}")
	public boolean deleteLibro( @PathVariable int id_libro) {
		
		return service.deleteBook(id_libro) ;
		
	}
	
	@PutMapping("/updateBook")
	public boolean updateLibro( @RequestBody Libro varLibro ) {
		
		return service.updateBook(varLibro) ;
		
	}
	
}

package com.gestione_libri.gestioneLibreria.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gestione_libri.gestioneLibreria.models.Categoria;
import com.gestione_libri.gestioneLibreria.service.CategoriaService;



@RestController
@RequestMapping("/categoria")
@CrossOrigin(origins="*", allowedHeaders="*")
public class CategoriaController {

	@Autowired
	private CategoriaService service ;
	
	@GetMapping("/test")
	public String test() {
		return "Hello World!" ;
	}
	
	@PostMapping("/inserisciCategoria")
	public Categoria aggiungiCategoria( @RequestBody Categoria varCategoria) {
		
		return service.saveCategoria(varCategoria);
		
	}
	
	@GetMapping("/{id_categoria}")
	public Categoria trovaCategoria( @PathVariable int id_categoria ) {
		
		return service.findyById(id_categoria) ;
		
	}
	
	@GetMapping("/")
	public List<Categoria> tutteLeCategorie(){
		
		return service.findAll() ;
		
	}
	
	@DeleteMapping("/{id_categoria}")
	public boolean eliminaCategoria( @PathVariable int id_categoria ) {
		
		return service.delete(id_categoria) ;
		
	}
	
	@PutMapping("/update")
	public boolean modifica_categoria(@RequestBody Categoria varCategoria) {
		
		return service.update(varCategoria) ;
		
	}
	
}
